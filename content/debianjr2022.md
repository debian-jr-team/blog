Title: Debian Jr. 2022
Slug: debian-jr-2022
Date: 2021-12-18 09:00
Author: Debian Jr. Team
Tags: 2022, junior

Debian Jr. is an internal project to make Debian an OS that children of all
ages will want to use. This project has as been started in the beginning of
2000. The initial focus was on producing something for children up to age 8
and the next target age range is 7 to 12. By the time children reach their
teens, they should be comfortable with using Debian without any special
modifications.

** In 2022 we are going to re-launch the Debian Jr. project.**

![DebianJrLogo](https://debian-jr-team.pages.debian.net/blog/images/DebianJr.jpg)

> Debian users are intelligent. They are adventuresome. They love to
> tinker, to explore. They are not afraid when something breaks, but know
> right away that the people who make this software will talk to them about
> their problem and help make it right. They have a strong "do it yourself"
> streak. They probably got this from their parents. They will probably
> pass it on to children in their care. 

Debian Jr. provides a collection of packages for children and their guides.
Beyond merely providing entertainment, the packages we have selected cover
a broad range of areas, from those which help the child visualize what is
inside their operating system to music, art, writing and programming as
well as some games and desktop toys. We hope that child and guides alike
will have fun, learn together, and make creative use of this excellent free
software.

> Ultimately, we owe the free software authors and maintainers the thanks for
> what they have given the world. Our work is to gather their work together
> into the Debian distribution, making it readily available to children and
> their guides. Once we select a package, we care for its quality, listening
> to the users, sending wishlists, bug reports, and fixes to Debian and
> upstream maintainers. It is this constant dialog between users and
> developers that ensures that the free software and our users continue to be
> our priorities, as we have promised in our [Social
> Contract](https://www.debian.org/social_contract).
>
> Finally, the [Social Contract](https://www.debian.org/social_contract) is
> key to understanding why we have embarked on this project at all. We hope
> that you will catch the spirit of this project, become excited by it, and
> by using Debian Jr. will not only benefit from what it has to offer as a
> system, but will also realize the benefits of entering into the global free
 >software community which we serve.

 * [Debian Jr. Wikipage](https://wiki.debian.org/DebianJr)
 * [Debian Jr. Mailinglist](https://lists.debian.org/debian-jr/)

